export type Message = {
    message: String;
    author: String;
    date: Date;
}
