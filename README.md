### How to run

`docker-compose up` in project root

`./mvnw spring-boot:run` in backend dir

`npm run start` in frontend dir

Browser opens automatically in `http://localhost:3000`
